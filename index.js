const express = require('express');
const axios = require('axios');

const app = express();

const host = process.env.HOST || '0.0.0.0';
const port = parseInt(process.env.PORT) || 3000;
const name = process.env.NAME || 'default'

app.get('/health', (req, res) => {
  res.send("I'm Alive!")
  console.log('/health')
})

app.get('/', (req, res) => {
  res.send(name)
  console.log(req.originalUrl)
})

app.get('/service', (req, res) => {
  if (req.query.url) {
    axios({
      method: 'get',
      url: req.query.url
    })
    .then(response => {
      res.send(response.data)
    })
    .catch(error => {
      res.send(error)
    })
  } else {
    res.send("Query string url cannot be undefined!")
  }

  console.log(req.originalUrl)
})

app.listen(port, host, () => {
  console.log(`running on host ${host} and port ${port}`)
})

// --------------------------------
// PROCESS ENVENTS
// --------------------------------
// catches ctrl+c event
process.on('SIGINT', () => {
  console.log(`SIGINT`);
  process.exit();
});
